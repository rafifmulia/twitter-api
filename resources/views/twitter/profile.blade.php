<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Twitter Profile</title>
    <link rel="stylesheet" href="{{ asset('bootstrap/css/bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('ui-kit/shards/css/shards.min.css') }}">
    <link rel="stylesheet" href="{{ asset('myown/css/profile.css') }}">
</head>
<body>
    
<div class="container">
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="row">
                    <div class="col-lg-5 col-md-6 col-sm-6">
                        <img src="{{ $foto }}" class="foto rounded d-block" alt="Photo Profile">
                    </div>
                    <div class="col-lg-7 col-md-6 col-sm-6 pic-blue">
                        <div class="card-body">
                            <!-- Heading -->
                            <div class="text-center">
                                <h4><strong class="text-info">{{ $name }}</strong></h4>
                                <small class="text-dark">{{ $screen_name }}</small>
                            </div>

                            <!-- Content -->
                            <div class="d-flex justify-content-around mt-5">
                                <div>
                                    <strong class="text-info">Following</strong>
                                    <p class="text-center text-dark">{{ $friends }}</p>
                                </div>
                                <div>
                                    <strong class="text-info">Followers</strong>
                                    <p class="text-center text-dark">{{ $followers }}</p>
                                </div>
                                <div>
                                    <strong class="text-info">Tweets</strong>
                                    <p class="text-center text-dark">{{ $tweet }}</p>
                                </div>
                            </div>

                            <!-- Footer -->
                            <div class="d-flex justify-content-center mt-5">
                                <button class="btn btn-info btn-lg rounded-40">Send Message</button>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<script src="{{ asset('jquery/jquery.js') }}"></script>
<script src="{{ asset('ui-kit/shards/js/shards.min.js') }}"></script>
<script src="{{ asset('bootstrap/js/bootstrap.bundle.min.js') }}"></script>
</body>
</html>
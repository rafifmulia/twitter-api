<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'Twitter\TwitterPage@login');

Route::prefix('/twitter')->group(function () {
    Route::get('/login', 'Twitter\TwitterPage@login');
    Route::get('/profile', 'Twitter\TwitterPage@profile');
});

Route::prefix('/twitter/oauth')->group(function () {
    Route::get('/login', 'Twitter\TwitterOauth@login');
    Route::get('/callback', 'Twitter\TwitterOauth@callback');
    Route::get('/test', 'Twitter\TwitterOauth@test');
});
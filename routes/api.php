<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
Route::middleware('auth.apiv1')->group(function() {
    Route::prefix('/v1')->group(function() {
        // perform oauth
        Route::get('/token', 'API_V1\TwitterAPI@token');
        
        // perform information
        Route::get('/credentials', 'API_V1\TwitterAPI@credentials');
        
        Route::get('/lists', 'API_V1\TwitterAPI@lists');
        Route::get('/lists/{sn}', 'API_V1\TwitterAPI@lists');

        Route::get('/followers', 'API_V1\TwitterAPI@followers');
        Route::get('/followers/{sn}', 'API_V1\TwitterAPI@followers');

        Route::get('/following', 'API_V1\TwitterAPI@following');
        Route::get('/following/{sn}', 'API_V1\TwitterAPI@following');

        Route::get('/mutual', 'API_V1\TwitterAPI@mutual');
        Route::get('/mutual/{sn}', 'API_V1\TwitterAPI@mutual');
        Route::get('/mutual/{sn}/{target_sn}', 'API_V1\TwitterAPI@mutual');

        Route::get('/mutuals', 'API_V1\TwitterAPI@mutuals');
        Route::get('/mutuals/{sn}', 'API_V1\TwitterAPI@mutuals');

        Route::get('/collections', 'API_V1\TwitterAPI@collections');
        Route::get('/collections/{sn}', 'API_V1\TwitterAPI@collections');

        // perform action
        Route::get('/follow', 'API_V1\TwitterAPI@follow');
        Route::get('/follow/{sn}', 'API_V1\TwitterAPI@follow');

        Route::get('/unfollow', 'API_V1\TwitterAPI@unfollow');
        Route::get('/unfollow/{sn}', 'API_V1\TwitterAPI@unfollow');
    });
});
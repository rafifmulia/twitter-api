<?php

namespace App\Http\Controllers\API_V1;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use Abraham\TwitterOAuth\TwitterOAuth;

class TwitterAPI extends Controller
{
    /**
     * PERFORM FUNCTION => can be used to another function
     */
    private function build() {
        return new TwitterOAuth(env('TWITTER_API_KEY'), env('TWITTER_API_SECRET_KEY'), env('TWITTER_ACCESS_TOKEN'), env('TWITTER_ACCESS_SECRET_TOKEN'));
    }

    protected function segments() {
        return explode('/', url()->current());
    }

    private function check_screen_name($req) {
        if ($req->sn == null) {
            header('Content-Type:application/json');
            echo json_encode(['message'=>'no screen name found']);
            exit;
        }
    }

    private function check_target_sn($req) {
        if ($req->target_sn == null) {
            header('Content-Type:application/json');
            echo json_encode(['message'=>'no target screen name found']);
            exit;
        }
    }

    /**
     * PERFORM OAUTH
     */
    public function token(Request $req) {
        $conn = new TwitterOAuth('hIYMaJTfzanoEoSWjlxxp587Z', 
        'FBxDPvzxBWz24CMiyicgLLwqwdRfy3CfXBGOnvBtvFnZmMbrEy');
        try {
            $content = $conn->oauth('oauth/request_token', array('oauth_callback' => 'http://127.0.0.1:8000'));
            // $content = $conn->oauth("oauth/access_token", ["oauth_verifier" => "nMznkpFRTMCuNMsmALzel9FgPlmWQDWg"]);
        } catch (\Abraham\TwitterOAuth\TwitterOAuthException $e) {
            // var_dump($e);
        } 
        $content = $conn->oauth('oauth/request_token', array('oauth_callback' => 'https://ramurous.web.id'));
        var_dump($content);
    }

    /**
     * PERFORM INFORMATION
     */
    public function credentials(Request $req) {
        $conn = $this->build();
        // $content = $conn->get('account/verify_credentials', ['include_email'=>'true']);
        $content = $conn->get('account/verify_credentials');
        
        header('Content-Type:application/json');
        echo json_encode($content);
    }

    public function lists(Request $req) {
        $this->check_screen_name($req);

        $conn = $this->build();
        $content = $conn->get('lists/list', ['screen_name'=>$req->sn]);

        header('Content-Type:application/json');
        echo json_encode($content);
    }

    public function followers(Request $req) {
        $this->check_screen_name($req);

        $conn = $this->build();
        $content = $conn->get('followers/list', ['cursor'=>'-1', 'screen_name'=>$req->sn, 'include_user_entities'=>false, 'count'=>'200']);
        
        header('Content-Type:application/json');
        echo json_encode($content);
    }

    public function following(Request $req) {
        $this->check_screen_name($req);

        $conn = $this->build();
        $content = $conn->get('friends/list', ['cursor'=>'-1', 'screen_name'=>$req->sn, 'include_user_entities'=>false, 'count'=>'200']);
        
        header('Content-Type:application/json');
        echo json_encode($content);
    }

    public function mutual(Request $req) {
        $this->check_screen_name($req);
        $this->check_target_sn($req);

        $conn = $this->build();
        $content = $conn->get('friendships/show', ['cursor'=>'-1', 'source_screen_name'=>$req->sn, 'target_screen_name'=>$req->target_sn]);
        
        header('Content-Type:application/json');
        echo json_encode($content);
    }

    public function mutuals(Request $req) {
        $this->check_screen_name($req);

        $conn = $this->build();
        $friends = $conn->get('friends/list', ['cursor'=>'-1', 'screen_name'=>$req->sn, 'include_user_entities'=>false, 'count'=>'200']);

        $mutuals = [];
        $separator = 0;
        $lookup[$separator] = '';

        foreach ($friends->users as $i => $user) {
            ++$i;
            $lookup[$separator] .= $user->screen_name.',';

            if ($i%100 == 0) {
                ++$separator;
                $lookup[$separator] = '';
                continue;
            }
        }
        
        foreach ($lookup as $div) {
            if ($div == '') continue;
            $mutuals[] = $conn->get('friendships/lookup', ['screen_name'=>$div]);
        }
        
        header('Content-Type:application/json');

        // $data = [];
        // foreach ($mutuals as $y => $users) {
        //     foreach ($users as $x => $user) {
        //         $data[$y][$x]['username'] = $user->screen_name;

        //         foreach ($user->connections as $status) {
        //             switch ($status) {
        //                 case 'followed_by':
        //                     $data[$y][$x]['status'] = 'You are mutual';
        //                     break;
        //                 default:
        //                     $data[$y][$x]['status'] = 'Not follback';
        //                     break;
        //             }
        //         }
        //     }
        // }

        echo json_encode($mutuals);
    }

    public function collections(Request $req) {
        $this->check_screen_name($req);

        $conn = $this->build();
        $content = $conn->get('collections/list', ['screen_name'=>$req->sn, 'count'=>'10', 'cursor'=>'-1']);

        header('Content-Type:application/json');
        echo json_encode($content);
    }


    /**
     * PERFORM ACTION
     */
    public function follow(Request $req) {
        $this->check_screen_name($req);

        $conn = $this->build();
        $content = $conn->post('friendships/create', ['screen_name'=>$req->sn, 'follow'=>false]);

        header('Content-Type:application/json');

        if ($content->following == false) {
            echo json_encode(['message'=>'success follow '.$req->sn]);
        } else {
            echo json_encode(['message'=>'you following '.$req->sn]);
        }
    }

    public function unfollow(Request $req) {
        $this->check_screen_name($req);

        $conn = $this->build();
        $content = $conn->post('friendships/destroy', ['screen_name'=>$req->sn]);

        header('Content-Type:application/json');

        if ($content->following == true) {
            echo json_encode(['message'=>'success unfollow '.$req->sn]);
        } else {
            echo json_encode(['message'=>'you not following '.$req->sn]);
        }
    }
}

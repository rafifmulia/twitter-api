<?php

namespace App\Http\Controllers\Twitter;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Abraham\TwitterOAuth\TwitterOAuth as oauth;

class TwitterOauth extends Controller
{
    private function build($oauth_token = '', $secret_token = '') {
        if ($oauth_token != '' || $secret_token != '') {
            return new oauth(env('TWITTER_API_KEY'), env('TWITTER_API_SECRET_KEY'), $oauth_token, $secret_token);
        }
        else {
            return new oauth(env('TWITTER_API_KEY'), env('TWITTER_API_SECRET_KEY'));
        }
    }

    public function login(Request $req) {
        $twit = $this->build();
        $request_token = $twit->oauth('oauth/request_token', ['oauth_callback'=>env('OAUTH_CALLBACK')]);

        if ($request_token['oauth_callback_confirmed'] !== 'true') {
            echo 'Request Token Failed : oauth callback not confirmed';
            exit;
        }

        header('Set-Cookie: oauth='.base64_encode(json_encode(['token'=>$request_token['oauth_token'],'secret'=>$request_token['oauth_token_secret']])).'; Path=/twitter/oauth; HttpOnly');

        $url = $twit->url('oauth/authorize', ['oauth_token'=>$request_token['oauth_token']]);
        
        header("location: ".$url);
        exit;
    }

    public function callback(Request $req) {
        if ($req->query('oauth_token') != null && isset($_COOKIE['oauth'])) {
            $oauth = json_decode(base64_decode($_COOKIE['oauth']));

            if (isset($oauth->token) && $oauth->token === $req->query('oauth_token')) {
                $twit = $this->build($oauth->token, $oauth->secret);
                $access_token = $twit->oauth('oauth/access_token', ['oauth_token'=>$req->query('oauth_token'), 'oauth_verifier'=>$req->query('oauth_verifier')]);
                
                header('Set-Cookie: oauth=; Path=/twitter/oauth; HttpOnly');
                header('Set-Cookie: credentials='.base64_encode(json_encode($access_token)).'; Path=/; HttpOnly');
                
                header("location: /twitter/profile");
                exit;
            }

            header("location: /twitter/oauth/login");
            exit;
        }
        else {
            header("location: /twitter/oauth/login");
            exit;
        }
    }

    public function test(Request $req) {
        var_dump($req->query());
    }
}

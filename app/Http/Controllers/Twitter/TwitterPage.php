<?php

namespace App\Http\Controllers\Twitter;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Abraham\TwitterOAuth\TwitterOAuth;

class TwitterPage extends Controller
{
    private function build() {
        if (!isset($_COOKIE['credentials']) && empty($_COOKIE['credentials'])) {
            header("location: /twitter/login");
            exit;
        }
        else {
            $user = json_decode(base64_decode($_COOKIE['credentials']));
            return new TwitterOAuth(env('TWITTER_API_KEY'), env('TWITTER_API_SECRET_KEY'), $user->oauth_token, $user->oauth_token_secret);
        }
    }

    public function login(Request $req) {
        return view('twitter.login');
    }

    public function profile(Request $req) {
        $twit = $this->build();
        $sn = $twit->get('account/verify_credentials');

        $foto = str_replace('_normal', '', $sn->profile_image_url);

        return view('twitter.profile', ['foto'=>$foto, 'name'=>$sn->name, 'screen_name'=>'@'.$sn->screen_name, 'followers'=>$sn->followers_count, 'friends'=>$sn->friends_count, 'tweet'=>$sn->statuses_count]);
    }
}
